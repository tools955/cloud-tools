package com.sinosoft.common.util.log.constant;

/**
 * 日志常量
 *
 * @author chenHui
 * @version 1.0.0
 */
public class LogConstant {
    /**
     * 服务链路跟踪标识
     */
    public static final String TRACE_ID = "traceId";
    /**
     * 服务名
     */
    public static final String MODULE_NAME = "moduleName";
    /**
     * 方法名
     */
    public static final String METHOD_NAME = "methodName";
}
