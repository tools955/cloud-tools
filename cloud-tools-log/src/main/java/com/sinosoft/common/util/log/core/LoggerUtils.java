package com.sinosoft.common.util.log.core;

import com.sinosoft.common.util.log.constant.LogConstant;
import org.slf4j.Logger;
import org.slf4j.MDC;

import java.util.Optional;

/**
 * 日志工具类
 *
 * @author chenHui
 * @version 1.0.0
 */
public class LoggerUtils {

    public LoggerUtils() {
        //
    }

    public static void info(Logger logger, String message, Object... params) {
        String log = getLog(getHeader(logger), message, params);
        logger.info(log);
    }


    public static void error(Logger logger, String message, Object... params) {
        String log = getLog(getHeader(logger), message, params);
        logger.error(log);
    }


    private static String getLog(String logHeader, String format, Object[] arg) {
        if (arg == null) {
            return !format.contains("{}") ? format : format.replaceFirst("\\{}", "null");
        } else {
            StringBuilder formatBuilder = (new StringBuilder(logHeader)).append(format);

            for (Object anArg : arg) {
                if (anArg instanceof Throwable) {
                    Throwable temp = (Throwable) anArg;
                    formatBuilder.append(temp).append("\n");
                    StackTraceElement[] stackTrace = temp.getStackTrace();

                    for (StackTraceElement aStackTrace : stackTrace) {
                        formatBuilder.append(aStackTrace).append("\n");
                    }
                } else {
                    formatBuilder = new StringBuilder(formatBuilder.toString().replaceFirst("\\{}", anArg == null ? "null" : anArg.toString()));
                }
            }

            format = formatBuilder.toString();
            return format;
        }
    }

    private static String getHeader(Logger logger) {
        try {
            String traceId = Optional.ofNullable(MDC.get(LogConstant.TRACE_ID)).orElse("");
            String moduleName = Optional.ofNullable(MDC.get(LogConstant.MODULE_NAME)).orElse("");
            String methodName = Optional.ofNullable(MDC.get(LogConstant.METHOD_NAME)).orElse("");
            String threadId = Optional.ofNullable(Thread.currentThread().getName()).orElse("");
            return String.format("traceId:【%s】,模块名称:【%s】,服务名: 【%s】,方法名:【%s】,执行线程名称:【%s】",
                    traceId, moduleName, logger.getName(), methodName, threadId);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
