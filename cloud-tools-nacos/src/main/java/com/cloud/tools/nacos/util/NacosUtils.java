package com.cloud.tools.nacos.util;

import cn.hutool.core.text.CharSequenceUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.cloud.tools.nacos.config.NacosConfig;
import com.cloud.tools.nacos.exception.NacosConfigException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Properties;

/**
 * nacos工具类
 *
 * @author chenHui
 * @version 1.0.0
 */
@Component
public class NacosUtils {
    @Resource
    private NacosConfig config;

    /**
     * 获取配置中心信息
     *
     * @param dataId 配置中心路径
     * @return 配置中心数据
     * @throws NacosConfigException nacos配置异常
     * @since 1.0.0
     */
    public String getNacosData(String dataId) {
        if (CharSequenceUtil.isEmpty(dataId)) {
            throw new NacosConfigException("dataId不能为空");
        }
        // 获取当前配置环境
        String nacosData = "";
        try {
            Properties properties = new Properties();
            properties.put(PropertyKeyConst.SERVER_ADDR, config.getServerAddr());
            properties.put(PropertyKeyConst.USERNAME, config.getUsername());
            properties.put(PropertyKeyConst.PASSWORD, config.getPassword());
            properties.put(PropertyKeyConst.NAMESPACE, config.getNamespace());
            ConfigService configService = NacosFactory.createConfigService(properties);

            nacosData = configService.getConfig(dataId, config.getGroup(), 3000);

        } catch (NacosException e) {
            e.printStackTrace();
        }
        if (CharSequenceUtil.isBlank(nacosData)) {
            throw new NacosConfigException(dataId, "配置为空");
        }
        return nacosData;
    }


    /**
     * 获取配置中心信息
     *
     * @param dataId 配置中心配置路径
     * @param clazz  转换的dto类型
     * @return 转换的dto集合
     * @throws NacosConfigException nacos配置异常
     * @since 1.0.0
     */
    public <T> T getNacosDataToDTO(String dataId, Class<T> clazz) {
        List<T> nacosDataToDTOList = getNacosDataToDTOList(dataId, clazz);
        if (nacosDataToDTOList.size() > 1) {
            throw new NacosConfigException(dataId, "配置大于1条");
        }
        return nacosDataToDTOList.get(0);
    }

    /**
     * 获取配置中心信息
     *
     * @param dataId 配置中心配置路径
     * @param clazz  转换的dto类型
     * @return 转换的dto集合
     * @throws NacosConfigException nacos配置异常
     * @since 1.0.0
     */
    public <T> List<T> getNacosDataToDTOList(String dataId, Class<T> clazz) {
        String nacosData = getNacosData(dataId);
        return JSON.parseArray(nacosData, clazz);
    }


    /**
     * 获取配置中心信息
     *
     * @param dataId 配置中心路径
     * @return JSONObject
     * @throws NacosConfigException nacos配置异常
     * @since 1.0.0
     */
    public JSONObject getConfigInfoMap(String dataId) {
        String jsonString = getNacosData(dataId);
        return JSON.parseObject(jsonString);
    }
}
