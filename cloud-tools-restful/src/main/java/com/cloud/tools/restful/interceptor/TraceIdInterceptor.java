package com.cloud.tools.restful.interceptor;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * traceId拦截器
 *
 * @author chenHui
 * @version 1.0.0
 */
@Component
public class TraceIdInterceptor implements HandlerInterceptor {
	private static final String TRACE_ID = "traceId";


	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		String traceId = request.getHeader(TRACE_ID);
		if (StringUtils.isEmpty(traceId)) {
			traceId = UUID.randomUUID().toString().trim().replace("-", "");
		}
		MDC.put(TRACE_ID, traceId);
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
		response.addHeader(TRACE_ID, MDC.get(TRACE_ID));
		MDC.remove(TRACE_ID);
	}
}
