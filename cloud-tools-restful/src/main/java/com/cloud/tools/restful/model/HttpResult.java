package com.cloud.tools.restful.model;

import com.cloud.tools.restful.enums.ResultCodeEnum;

import java.io.Serializable;

/**
 * restful统一返回出参
 *
 * @author chenHui
 * @version 1.0.0
 */
public class HttpResult implements Serializable {
    /**
     * 响应状态码
     */
    private Integer code;
    /**
     * 错误信息
     */
    private String msg;
    /**
     * 响应数据
     */
    private Object data;

    public static HttpResult success(Object data) {
        HttpResult result = new HttpResult();
        result.data = data;
        result.code = ResultCodeEnum.SUCCESS.getCode();
        result.msg = "成功";
        return result;
    }

    public static HttpResult failure(String msg) {
        HttpResult result = new HttpResult();
        result.code = ResultCodeEnum.SERVER_ERROR.getCode();
        result.msg = msg;
        return result;
    }


    public static HttpResult failure(ResultCodeEnum resultCodeEnum) {
        HttpResult result = new HttpResult();
        result.code = resultCodeEnum.getCode();
        result.msg = resultCodeEnum.getMessage();
        return result;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
