package com.cloud.tools.restful.vaildated;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * 参数验证框架配置
 * 处理controller校验，开启快速校验，判断到有一个校验不通过就返回
 *
 * @author chenHui
 * @version 1.0.0
 */
@Configuration
public class ValidatorConfig {
	@Bean
	public Validator validator() {
		ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
				.configure()
				// true开启快速校验，判断到有一个校验不通过就返回
				.failFast(true)
				.buildValidatorFactory();
		return validatorFactory.getValidator();
	}
}
